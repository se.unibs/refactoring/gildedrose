package com.gildedrose;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SnaphotTest {
    @Test
    public void shouldGenerateExpectedOutput() {
        var output = new ByteArrayOutputStream();
        var original = System.out;
        PrintStream outStream = new PrintStream(output);
        System.setOut(outStream);
        TextTestFixture.main(new String[] {});
        outStream.flush();
        Approvals.verify(output.toString());
        System.setOut(original);
    }
}
